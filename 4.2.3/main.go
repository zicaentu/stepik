package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
)

func main() {
	name, age := getUserInput()
	responseBody, err := sendHTTPRequest(name, age)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(responseBody)
}

func getUserInput() (string, string) {
	var name, age string
	fmt.Scan(&name, &age)
	return name, age
}

func sendHTTPRequest(name string, age string) (string, error) {
	base, err := url.Parse("http://127.0.0.1:8080")
	if err != nil {
		log.Println(err)
		return "", err
	}
	base.Path += "hello"
	params := url.Values{}
	params.Add("name", name)
	params.Add("age", age)

	base.RawQuery = params.Encode()

	resp, err := http.Get(base.String())
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(responseBody), nil
}

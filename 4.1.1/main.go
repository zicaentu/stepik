package main

import (
	"container/list"
	"fmt"
)

func Push(elem interface{}, queue *list.List) {
	queue.PushFront(elem)
}

func Pop(queue *list.List) interface{} {
	if queue.Len() == 0 {
		return nil
	}

	// elem := queue.Back().Value

	return queue.Remove(queue.Back())
}

func printQueue(queue *list.List) {
	for elem := queue.Back(); elem != nil; elem = elem.Prev() {
		fmt.Printf("%v ", elem.Value)
	}
	fmt.Println()
}

func main() {
	queue := list.New()
	Push(1, queue)
	Push("two", queue)
	Push(3.14, queue)
	printQueue(queue)
	elem := Pop(queue)
	fmt.Println(elem)
	printQueue(queue)
	Pop(queue)
	Pop(queue)
	Pop(queue)
	printQueue(queue)
}

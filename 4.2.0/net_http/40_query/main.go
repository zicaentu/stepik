package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
)

func main() {
	// Создаем URL с параметрами
	baseURL := "https://example.com/api/resource"
	params := url.Values{}
	params.Add("param1", "value1")
	params.Add("param2", "value2")

	fullURL := baseURL + "?" + params.Encode()

	// Отправляем GET-запрос
	response, err := http.Get(fullURL)
	if err != nil {
		fmt.Println("Ошибка при отправке GET-запроса:", err)
		return
	}
	defer response.Body.Close()

	// Чтение ответа
	// ...

	// Обработка ответа
	// ...
}

func queryConstruct() {
	// Парсим базовый URL "https://www.example.com"
	base, err := url.Parse("https://www.example.com")
	if err != nil {
		log.Println(err)
		return
	}

	// Добавляем путь к базовому URL, создавая "https://www.example.com/path"
	base.Path += "path"

	// Создаем query параметры запроса
	params := url.Values{}
	params.Add("id", "15")
	params.Add("name", "Dima")

	// Кодируем параметры запроса в строку и устанавливаем как часть запроса в URL
	base.RawQuery = params.Encode()

	// Выводим итоговый URL
	fmt.Printf("Encoded URL is %q\n", base.String())
}

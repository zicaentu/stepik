package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	connection, err := net.Dial("tcp", "0.0.0.0:8081")
	if err != nil {
		log.Println(err)
		return
	}

	buffer := make([]byte, 1024)
	n, err := connection.Read(buffer)
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Println(string(buffer[:n]))
}

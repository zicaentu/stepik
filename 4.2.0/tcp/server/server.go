package main

import (
	"log"
	"net"
	"time"
)

func main() {
	// listener, err := net.Listen("tcp", "0.0.0.0:8081")
	listener, err := net.ListenTCP("tcp", &net.TCPAddr{IP: net.IPv4zero, Port: 8081})

	if err != nil {
		log.Println(err)
		return
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Println(err)
			continue
		}
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()

	message := []byte(time.Now().String())

	_, err := conn.Write(message)
	if err != nil {
		log.Println(err)
	}
	log.Println("message sent")
}

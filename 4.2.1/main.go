package main

import (
	"fmt"
	"log"
	"net"
	"strings"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8081")
	if err != nil {
		log.Fatal(err)
		return
	}

	buffer := make([]byte, 1024)

	for i := 0; i < 3; i++ {
		n, err := conn.Read(buffer)
		if err != nil {
			log.Fatal(err)
			return
		}
		fmt.Println(strings.ToUpper(string(buffer[:n])))
		clear(buffer)
	}
}

package main

import (
	"container/list"
	"fmt"
)

// ReverseList - функция для реверса списка
func ReverseList(l *list.List) *list.List {
	for first, last := l.Front(), l.Back(); first != last; first, last = first.Next(), last.Prev() {
		first.Value, last.Value = last.Value, first.Value
	}
	return l
}

func printQueue(queue *list.List) {
	for elem := queue.Back(); elem != nil; elem = elem.Prev() {
		fmt.Printf("%v ", elem.Value)
	}
	fmt.Println()
}

func main() {
	l := list.New()
	l.PushBack(1)
	l.PushBack("two")
	l.PushBack(3.14)
	l.PushBack(4)
	l.PushBack(5)
	printQueue(l)
	l = ReverseList(l)
	printQueue(l)
}
